import 'package:flutter/material.dart';
import 'package:flutter_app4/users/userList.dart';

void main() => runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyApp(),
));