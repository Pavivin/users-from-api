import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserPage extends StatefulWidget {
  final int index;
  final String name;
  final String image;

  const UserPage({Key key,
    @required this.index,
    @required this.name,
    @required this.image

  }) : super(key: key);

  @override
  Info createState() => Info();
}

class Info extends State<UserPage> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(widget.name)),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Image.network(
              widget.image,
            fit: BoxFit.cover,
          ),
        ),
      )
    );
  }
}