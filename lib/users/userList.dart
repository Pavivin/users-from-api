import 'package:flutter_app4/users/userPage.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'dart:convert';

import 'user.dart';
import '../token.dart' show url;

class MyApp extends StatefulWidget {
  @override _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Widget listViewBuilder(BuildContext context, List<dynamic> values) {
    return ListView.builder(
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return Hero(
            tag: values[index].id,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: ListTile(
                title: Row(
                  children: <Widget>[
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(values[index].image)
                          )
                      ),
                    ),
                    SizedBox(width: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                            width: MediaQuery.of(context).size.width / 2,
                            child: Text(values[index].name,
                                style: TextStyle(fontSize: 17))),
                        SizedBox(height: 10),
                      ],
                    )
                  ],
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => InfoPage(
                      index: index,
                      name: values[index].name,
                      image: values[index].image,
                    )),
                  );
                },
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder(
            future: obtainJson(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  return Text('Press button to start.');
                case ConnectionState.active:
                  return Center(child: CircularProgressIndicator());
                case ConnectionState.waiting:
                  return Center(child: CircularProgressIndicator());
                case ConnectionState.done:
                  if (snapshot.hasError)
                    return Text('Error: ${snapshot.error}');
                  return listViewBuilder(context, snapshot.data);
                default:
                  return Text('Some error occurred');
              }
            }
        )
    );
  }

  Future<List<User>> obtainJson() async {
    final response = await http.get(url);
    dynamic jsonObject = json.decode(response.body.toString());
    final convertedJsonObject = jsonObject.cast<Map<String, dynamic>>();
    List<User> list = convertedJsonObject.map<User>((json) =>
        User.fromJson(json)).toList();
    return list;
  }
}
