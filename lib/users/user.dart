import 'package:flutter/material.dart';

class User {
  final String id;
  final String name;
  final String image;

  const User({
    @required this.id,
    @required this.name,
    @required this.image});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      name: json['user']['name'],
      image: json['user']['profile_image']['large'],
    );
  }
}